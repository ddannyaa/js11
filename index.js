let iconPassword = document.querySelectorAll(".icon-password");

    iconPassword.forEach(function(icon) {
    icon.addEventListener("click", function() {
        const target = icon.getAttribute("data-type");
        let inputPassword = document.querySelector(target);
        if (inputPassword.type === "password") {
            inputPassword.type = "text";
            icon.classList.replace("fa-eye", "fa-eye-slash") ;
        } else {
            inputPassword.type = "password";
            icon.classList.replace("fa-eye-slash", "fa-eye");
        }
    });
}); 


const btnConfirm = document.querySelector(".btn");

btnConfirm.addEventListener("click", (ev) => {
  const password = document.getElementById("input-password").value;
  const ConfirmPassword = document.getElementById("confirm-password").value;

  ev.preventDefault();

 if (password === ConfirmPassword) {
    alert("You are welcome!!!!");
  }
  else {
    errorOutput("Потрібно ввести однакові значення!");
  }
});

function errorOutput(message) {
  const btn = document.querySelector(".btn");
    const attention = document.createElement("span");

  if (document.querySelector(".error-text") === null) {
    attention.classList.add("error-text");
    attention.innerHTML = message;
    btn.before(attention);
    setTimeout(() => attention.remove(), 3000);
  }
}
   
